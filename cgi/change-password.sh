#!/bin/bash

function urldecode() { : "${*//+/ }"; echo -e "${_//%/\\x}"; }

echo 'Content-type: text/html'
echo
echo
echo '<body>'
echo '<div style="text-align: center">'

read -n $CONTENT_LENGTH CONTENT
for PARAM in $(echo "$CONTENT" | sed 's/&/ /g'); do
        FIELD=$(echo $PARAM | cut -d '=' -f  1)
        VALUE=$(urldecode $(echo $PARAM | cut -d '=' -f  2))
        if [ 'username' == "$FIELD" ]; then
                KRB5_USERNAME=$VALUE
        elif [ 'password1' == "$FIELD" ]; then
                KRB5_PASSWORD1=$VALUE
        elif [ 'password2' == "$FIELD" ]; then
                KRB5_PASSWORD2=$VALUE
        elif [ 'password3' == "$FIELD" ]; then
                KRB5_PASSWORD3=$VALUE
        fi
done

OUTPUT=$(echo "$KRB5_PASSWORD1
$KRB5_PASSWORD2
$KRB5_PASSWORD3
" | kpasswd $KRB5_USERNAME 2>&1)

RETURN_CODE=$?

if [ "0" == "$RETURN_CODE" ]; then
        echo "Password changed successfully"
else
        echo "Error while changing password"
fi
echo '<br><br>'
echo "$OUTPUT" | tail -n 1
echo '<br><br>'
echo '<a href="../index.html">Return to form</a>'

echo '</div>'
echo '</body>'
