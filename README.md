# web-kpasswd

Simple web application allowing to change Kerberos password.
***

## Description
Sometime Kerberos is used for authentification by applications with username and password, instead of native Kerberos tools and protocol (i.e. kinit) and it's not always possible to ask users that want to change their password to install kpasswd and edit configuration files, just to do it. With this application a user can use a simple web browser to change his password.
In order to work, this application needs the kpasswd command on the server hosting it.

## Installation
This is a simple application that should normally run on any web server with CGI support and which is able to run a BASH script. Here is an example of how to deploy it on a Debian (Bullseye) server with NginX as WEB server and the kerberos realm EXAMPLE.COM, managed by the KDC whose hostname is kerberos.example.com. These values should be updated according to your environment.

First install Kerberos client

```bash
# apt install krb5-user
```

During the install you'll be prompted to provide your default Kerberos REALM, KDC and Kerberos administration server. Enter respectively EXAMLPLE.COM, kerberos.example.com and kerberos.example.com.

After that install NginX WEB server and FCGIWrap

```bash
# apt install nginx fcgiwrap
```

Install web-kpasswd and make sure the CGI script is executable.

```bash
# apt install git
# cd /var/www/html
# git clone https://gitlab.com/yasalos/web-kpasswd.git
# chmod a+x web-kpasswd/cgi/change-password.sh
```

If you want, rename the folder web-passwd according to the context you want to use to access the application. In this example, we want to make the application available at the following url https://myhost/password, so we will use the command below:

```bash
# mv web-kpasswd password
```

Now, configure Nginx by adding the following lines in server section of the host through which you want to access the application.

```
location /password/cgi/ {
        gzip off;
        root  /var/www/html;
        fastcgi_pass  unix:/var/run/fcgiwrap.socket;
        include /etc/nginx/fastcgi_params;
        fastcgi_param SCRIPT_FILENAME  /var/www/html$fastcgi_script_name;
}
```

** The application doesn't check if it's accessed through SSL, so for security purpose, you must make sure to deploy it only on HTTPS. **

Restart Nginx to take into account the new configuration.

```bash
# systemctl restart nginx
```

Access to the password change form at the URL https://myhost/password.

## Usage
The only available form of the application is simple and straight forward, with 4 fields:
* Username : the username of the user who want to change password. You can use a username without specifying the realm if he belongs to the default realm, otherwhise you must provide the full principal (e.g. user@EXAMPLE.COM).
* Current password : the current password of the user.
* New password : the new password of the user.
* Confirm new password : the new password of the user (must be identical to the value of the previous field).

Just fill them as described above and click on the button "Update password".

## License
[GPL3](https://www.gnu.org/licenses/gpl-3.0.html)
